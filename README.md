# Codesenberg Weather App

Weather Application based off https://github.com/brechtbilliet/angular-typescript-webpack. Uses json-server to load in city list data for predictive searching (please read install instructions).

### Install

```sh
//make sure port 8080 and 3000 are both free
npm i webpack typings typescript json-server -g
git clone https://codesenberg@bitbucket.org/codesenberg/codesenberg-weather-app.git
cd codesenberg-weather-app
npm install
npm run json-server
npm start
```

Then it will automatically open the app in your browser

To run tests

```sh
npm test
```

Coverage

```sh
open reports/coverage/index.html
```

Build
```sh
npm install
npm run build
```


### Features

- [x] Build basic Angular app with webpack
- [x] Simple twitter application
- [x] fully tested with Jasmine
- [x] sass support
- [x] Coverage report
- [x] Typescript support
- [x] ES6 modules support
- [x] Running tests in PhantomJS
- [x] Wallaby.js support
- [x] Karma support
- [x] Optimized build package
- [x] Minimal and straightforward setup
- [x] Watches code and refreshes browser with latest changes automatically
- [x] Sourcemap support in develop AND PRODUCTION!!! (don't deploy the js.map file with your application, but use it to debug your production app)
