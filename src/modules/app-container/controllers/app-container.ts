import {AppContainerController} from './app-container.controller';

/** 
 * @description App Container
 */
export class AppContainer {
    public bindings: any;
    public template: any;
    public controller: any;

    constructor() {
        this.bindings = {};
        this.controller = AppContainerController;
        this.template =  require('../views/app-container.html');
    }
}
