import {} from 'jasmine';
import 'angular-mocks';
import * as angular from 'angular';

describe('Component: Test', () => {
    let $compile: any;
    let $rootScope: any;
    let element: any;

    beforeEach(angular.mock.module('app'));
    //TODO get inject to work correctly
    beforeEach(angular.mock.inject(function (_$compile_: any, _$rootScope_: any): void {
        $compile = _$compile_;
        $rootScope = _$rootScope_;
    }));

    beforeEach(() => {
        element = $compile('<app-container>')($rootScope);
        $rootScope.$digest();
    });
    /*
    it('should say hello to the world', () => {
        expect(element.find('div').text()).toEqual('Hello, World');
    });

    it('component.test should equal test', () => {
        expect(element.controller('appContainer').test).toBe('test');
    });*/
});