import { find as _find } from 'lodash';
const Promise: any = require('bluebird');
const config: any = require('../stores/forecast-configs');
export class AppContainerController {
    public forecastItems: any[];
    public currentForecast: ForecastItem;
    public city: any;
    public temparuateMeasurement: string = 'celsius';
    public temparuateMeasurementSymbol: string;
    public cityId: number;
    public cityName: string;
    public cityList: any;
    public errorMessage: string;
    public isSearching: boolean;
    public dataLoaded: boolean;
    public cityListPromise: Promise<any>;
    private _invalidIdError: string = 'Please enter valid country id. Ids can be found here: http://bulk.openweathermap.org/sample/ (city.list.json.gz). Or make sure json server is running, check README for instructions.';
    /*@ngInject*/
    constructor(public $forecastService: any, public $api: any, public $timeout: any, public $sce: any) {
        //configure celsius symbol
        this.temparuateMeasurementSymbol = this.$sce.trustAsHtml('&#8451;');
        //load city list data
        this.cityListPromise = new Promise((resolve, reject) => {
            this.$api.getRequest(config.CITY_LIST_URL).then((data) => {
                resolve(data);
                this.$timeout(() => {
                    this.cityList = data;
                });
            });
        });

        //bind to scope
        this.findWeather = this.findWeather.bind(this);
        this._validateSearch = this._validateSearch.bind(this);
    }

    /** 
     * @description toggles temperature changing from celsius to fahrenheit 
     */
    public onTemperatureChange(): void {
        if (this.currentForecast.main.temp) {
            if (this.temparuateMeasurement === 'celsius') {
                //switching from fahrenheit to celsius
                this.currentForecast.main.temp = this.$forecastService.fahrenheitToCelsius(this.currentForecast.main.temp);
                this.forecastItems.forEach((o) => {
                    o.main.temp = this.$forecastService.fahrenheitToCelsius(o.main.temp);
                });
                this.temparuateMeasurementSymbol = this.$sce.trustAsHtml('&#8451;');
            } else if (this.temparuateMeasurement === 'fahrenheit') {
                //switching from celsius to fahrenheit
                this.currentForecast.main.temp = this.$forecastService.celsiusToFahrenheit(this.currentForecast.main.temp);
                this.forecastItems.forEach((o) => {
                    o.main.temp = this.$forecastService.celsiusToFahrenheit(o.main.temp);
                });
                this.temparuateMeasurementSymbol = this.$sce.trustAsHtml('&#8457;');
            }
        }
    }

    /** 
     * @description loads weather data if search is valid
     */
    public findWeather(): void {
        this.isSearching = true;
        this.dataLoaded = false;
        this.$timeout(() => {
            this.cityListPromise.then((data) => {
                //once city list data load has been resolved validate search
                if (this._validateSearch(data)) {
                    //once search has been validated, do api call for weather data
                    this.$forecastService.get5dayForecast(this.cityId, this.temparuateMeasurement).then((forecastData: ForecastData) => {
                        console.log(forecastData, 'what is this forecastData');
                        this.$timeout(() => {
                            if (forecastData) {
                                //if api call is good, load data
                                this.city = forecastData.city;
                                this.currentForecast = forecastData.current;
                                this.forecastItems = forecastData.get5day;
                                this.dataLoaded = true;
                            } else {
                                //api call fails
                                this.errorMessage = this._invalidIdError;
                            }
                            this.isSearching = false;
                        });
                    });
                } else {
                    //search is invalid
                    this.isSearching = false;
                }
            });
        });
    }

    /** 
     * @description validates current search
     * @return whether search is valid or not
     */
    private _validateSearch(data: any): boolean {
        let isValid: boolean;

        if (!data) {
            //no data loaded from city list so just verify input isnt empty
            isValid = this.cityName.length > 0;
            if (!isValid) {
                this.errorMessage = this._invalidIdError;
            } else {
                this.cityId = Number(this.cityName);
            }
        } else if (!this.cityId) {
            //else verify that current input returns a valid city id
            isValid = this._validateCityId(this.cityName, data);
            if (!isValid) {
                this.errorMessage = 'City not found, please enter valid city';
            }
        } else {
            isValid = true;
        }

        if (isValid) {
            this.errorMessage = '';
        }

        return isValid;
    }

    /** 
     * @description validates current cityId if city list has been provided by finding corresponding city id based off of city name
     * @return whether city id is valid or not
     */
    private _validateCityId(cityName: string, data: any): boolean {
       if (cityName.trim() !== '') {
            const city: any = _find(data, (o) => { return o.name.toLowerCase() === cityName.toLowerCase().trim(); });
            //updates current city id if it has not been updated yet
            this.cityId = (city) ? city.id : undefined;
            return this.cityId !== undefined;
        }
        return false;
    }
}
