import {} from 'jasmine';
import * as moment from 'moment';
import {ForecastService} from './forecast.service';
import {APIService} from '../../api';

describe('Forecast Service: Test', () => {
    let $forecastService: any;
    let data: ForecastItem;
    let rainAccumulationMap: any = {};

    beforeEach(() => {
        $forecastService = new ForecastService(new APIService());
    });

    beforeEach(() => {
        data = {
            dateToString: '',
            wind: <WindObj>{
                deg: 30,
                speed: 30
            },
            rainAccumulation: 0,
            dt: 1502107200,
            dt_txt: '2017-08-07 12:00:00',
            main: {},
            weather: {},
            rain: {
                '3h': 10
            }
        };
        rainAccumulationMap[data.dt_txt.slice(0, 10)] = {
            rain: 30
        };
    });

    it('get 5 day weather data api call', () => {
        expect($forecastService.get5dayForecast(707860)).toBeDefined();
    });

    it('format 5 day weather data', () => {
        const dataListTomorrow: any = [];
        const tomorrow: any = moment(new Date()).add(1, 'days');
        data.dt = tomorrow.unix();
        data.dt_txt = tomorrow.format('YYYY-MM-DD') + ' 12:00:00';
        dataListTomorrow.push(data);
        //if not today
        expect($forecastService.format5day(dataListTomorrow, rainAccumulationMap).length).toBe(1);

        const dataListToday: any = [];
        const today: any = moment(new Date());
        data.dt = today.unix();
        data.dt_txt = today.format('YYYY-MM-DD') + ' 12:00:00';
        dataListToday.push(data);

        //if today's date
        expect($forecastService.format5day(dataListToday, rainAccumulationMap).length).toBe(0);
    });

    it('format current weather item', () => {
        const formatted: ForecastItem = $forecastService.formatCurrent(data, rainAccumulationMap);
        expect(formatted.dateToString).toEqual('Mon Aug 7');
        expect(formatted.wind.cardinal).toEqual('NE');
        expect(formatted.rainAccumulation).toEqual(30);
        expect(formatted.wind.speed).toEqual(108);
    });

    it('fahrenheit to convert to celsius with 2 decimal places', () => {
        const temperature: number = 80;
        const celsius: number = ((temperature) - 32) * 5 / 9;
        expect($forecastService.fahrenheitToCelsius(temperature)).toEqual(Number(celsius.toFixed(2)));
    });

    it('accumalate rain over 24 hours based on weather data', () => {
        const dataList: any = [];
        dataList.push(data);
        dataList.push(data);
        dataList.push(data);
        expect($forecastService._formatRainAccumulation(dataList)[data.dt_txt.slice(0, 10)].rain).toEqual(30);
    });

    it('get cardinal directions based off of angle', () => {
        const angleN: number = 0;
        const angleNE: number = 35;
        const angleE: number = 90;
        const angleSE: number = 145;
        const angleS: number = 180;
        const angleSW: number = 220;
        const angleW: number = 270;
        const angleNW: number = 330;
        expect($forecastService._getCardinal(angleN)).toEqual('N');
        expect($forecastService._getCardinal(angleNE)).toEqual('NE');
        expect($forecastService._getCardinal(angleE)).toEqual('E');
        expect($forecastService._getCardinal(angleSE)).toEqual('SE');
        expect($forecastService._getCardinal(angleS)).toEqual('S');
        expect($forecastService._getCardinal(angleSW)).toEqual('SW');
        expect($forecastService._getCardinal(angleW)).toEqual('W');
        expect($forecastService._getCardinal(angleNW)).toEqual('NW');
    });
});