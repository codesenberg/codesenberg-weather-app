import { has as _has } from 'lodash';
import { get as _get } from 'lodash';
import * as moment from 'moment';
const Promise: any = require('bluebird');
const config: any = require('../stores/forecast-configs');
const mockData: any = require('../stores/mockdata');

/**
 * @description Handles forecast data request and data formatting
 */
export class ForecastService {
    /*@ngInject*/
    constructor(public $api: any) {}

    /** 
     * @description gets the current forecast and 5 day forecast
     * @param cityId defines city id
     * @param temparuateType defines whether to search in celsius or fahrenheit
     * @return Promise contain forecast data
     */
    public get5dayForecast(cityId: number, temparuateType: string = 'celsius'): Promise<any> {
        const useMock: boolean = false;
        if (!useMock) {
            const urlKey: string = config.API_KEY;
            const temparuateTypeUnit: string = (temparuateType !== 'celsius') ? 'imperial' : 'metric';
            const url: string = config.API_URL + `?id=${cityId}&APPID=${urlKey}&units=${temparuateTypeUnit}`;

            //Real API Call
            return this.$api.getRequest(url).then((data) => {
                let result: ForecastData;
                if (data.cod === '200') {
                    const rainAccumulationMap: any = this._formatRainAccumulation(data.list);
                    result = {
                        city: data.city,
                        current: this.formatCurrent(data.list[0], rainAccumulationMap),
                        get5day: this.format5day(data.list, rainAccumulationMap)
                    };
                }
                //getting returning data in a promise to avoid unnecesary complexity
                return result;
            });
        } else {
            //Mock API Call
            return new Promise((resolve, reject) => {
                const rainAccumulationMap: any = this._formatRainAccumulation(mockData.list);
                const result: ForecastData = {
                    city: mockData.city,
                    current: this.formatCurrent(mockData.list[0], rainAccumulationMap),
                    get5day: this.format5day(mockData.list, rainAccumulationMap)
                };
                resolve(result);
            });
        }
    }

    /** 
     * @description formats weather data object
     * @param current defines data being formatted
     * @param rainAccumulationMap defines a map which determines the rain accumatlion for that day
     * @return formatted data
     */
    public formatCurrent(current: ForecastItem, rainAccumulationMap: any): ForecastItem {
        let rainAccumulation: number = _get(rainAccumulationMap[current.dt_txt.slice(0, 10)], 'rain');
        current.dateToString = moment.unix(current.dt).format('ddd MMM D');
        current.wind.cardinal = this._getCardinal(current.wind.deg);
        current.rainAccumulation =  (rainAccumulation !== undefined) ? Math.round(rainAccumulation) : rainAccumulation;
        current.wind.speed = Number((current.wind.speed * 3.6).toFixed(2));
        return current;
    }

    /** 
     * @description formats 5 day weather data
     * @param data defines list of weather data to be formatted
     * @param rainAccumulationMap defines a map which determines the rain accumatlion for that day
     * @return formatted data
     */
    public format5day(data: any[], rainAccumulationMap: any): any[] {
        const today: any = moment();
        const formattedData: any[] = data.filter((weatherObj: ForecastItem) => {
            const isToday: any = today.isSame(moment.unix(weatherObj.dt), 'd');
            return !isToday && weatherObj.dt_txt.indexOf('12:00:00') !== -1;
        }).map((weatherObj: any) => {
            return this.formatCurrent(weatherObj, rainAccumulationMap);
        });
        return formattedData;
    }

    /** 
     * @description converts fahrenheit to celsius
     * @param temperature defines temperature in fahrenheit
     * @return temperature in celsius
     */
    public fahrenheitToCelsius(temperature: number): number {
        //(T(°F) - 32) × 5/9
        return Number(((temperature - 32) * 5 / 9).toFixed(2));
    }

    /** 
     * @description converts celsius to fahrenheit 
     * @param temperature defines temperature in celsius
     * @return temperature in fahrenheit
     */
    public celsiusToFahrenheit(temperature: number): number {
        //T(°C) × 9/5 + 32
        return Number((temperature * 9 / 5 + 32).toFixed(2));
    }

    /** 
     * @description creates a map of rain accummulation for each day 
     * @param data defines list of weather data
     * @return rain accumalation map
     */
    private _formatRainAccumulation(data: any[]): any {
        const rainAccumulationMap: any = {};
        data.forEach((o) => {
            if (_has(o, 'rain.3h')) {
                const currentDate: string = o.dt_txt.slice(0, 10);
                if (rainAccumulationMap[currentDate] === undefined) {
                    rainAccumulationMap[currentDate] = {rain: 0};
                }
                rainAccumulationMap[currentDate].rain += o.rain['3h'];
            }
        });
        return rainAccumulationMap;
    }

    /** 
     * @description given "0-360" returns the nearest cardinal direction "N/NE/E/SE/S/SW/W/NW/N"; https://gist.github.com/basarat/4670200
     * @param angle defines angle in degrees
     * @return returns cardinal direction
     */
    private _getCardinal(angle: number): string {
        //easy to customize by changing the number of directions you have 
        const directions: number = 8;
        const degree: number = 360 / directions;
        angle = angle + degree / 2;

        if (angle >= 0 * degree && angle < 1 * degree) {
            return 'N';
        }
        if (angle >= 1 * degree && angle < 2 * degree) {
            return 'NE';
        }
        if (angle >= 2 * degree && angle < 3 * degree) {
            return 'E';
        }
        if (angle >= 3 * degree && angle < 4 * degree) {
            return 'SE';
        }
        if (angle >= 4 * degree && angle < 5 * degree) {
            return 'S';
        }
        if (angle >= 5 * degree && angle < 6 * degree) {
            return 'SW';
        }
        if (angle >= 6 * degree && angle < 7 * degree) {
            return 'W';
        }
        if (angle >= 7 * degree && angle < 8 * degree) {
            return 'NW';
        }

        //Should never happen: 
        return 'N';
    }

}