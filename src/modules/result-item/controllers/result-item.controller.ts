/**
 * @description Controller for ResultItem component
 */
export class ResultItemController {
    public cityName: string;
    public weatherDescription: string;
    public iconSrc: string;
    public temperature: number;
    public temperatureMeasurementSymbol: string;
    public currentRain: number;
    public rainOverFullDay: number;
    public windSpeed: number;
    public windCardinal: string;
    public humidity: number;
    public type: string;
    public currentDate: string;
    public temperatureMeasurementSymbolTrusted: any;

    /*@ngInject*/
    constructor(public $element: any, public $timeout: any, public $compile: any, public $scope: any) {

        //dynamically load current template or future template
        let container: any = this.$element.find('#result-item');
        let elem: any;

        $timeout(() => {
            if (this.type === 'current') {
                elem = $compile(require('../views/result-item-current.html'))($scope.$new());
            } else {
                elem = $compile(require('../views/result-item-future.html'))($scope.$new());
            }
            container.html(elem);
        });
    }
}
