import {ResultItemController} from './result-item.controller';
/**
 * @description Displays the results of weather forecast search
 */
export class ResultItem {
    public bindings: any;
    public template: any;
    public controller: any;

    /** 
     * @description Bindings
     * cityName {string} defines the city name
     * weatherDescription {string} defines weather description for that day
     * iconSrc {string} defines url to icon representing that day's weather
     * temperature {number} defines current temperature in degrees
     * temperatureMeasurementSymbolTrusted {any} defines farenheit or celcius symbol wrapped in $sce object
     * currentRain {number} defines rainfall over 3hrs in mm
     * rainOverFullDay {number} defines rainfall over that day's 24 period
     * windSpeed {number} defines wind speed in km/h
     * windCardinal {string} defines the wind direction
     * humidity {number} defines the wind humidity in degrees
     * currentDate {string} defines the current date
     * type {string} expects current or future; defines what type of template to use
     */
    constructor() {
        this.bindings = {
            cityName: '<',
            weatherDescription: '<',
            iconSrc: '<',
            temperature: '<',
            temperatureMeasurementSymbolTrusted: '<',
            currentRain: '<',
            rainOverFullDay: '<',
            windSpeed: '<',
            windCardinal: '<',
            humidity: '<',
            currentDate: '<',
            type: '@'
        };
        this.controller = ResultItemController;
        this.template =  '<div id="result-item"></div>';
    }
}
