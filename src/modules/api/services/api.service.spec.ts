import {} from 'jasmine';
require('whatwg-fetch');

import {APIService} from './api.service';

describe('Api Service: Test', () => {
    let $api: any;

    beforeEach(() => {
        $api = new APIService();
    });

    it('getRequest should return a fetch promise', () => {
        const url: string = 'http://localhost:8080/';
        expect($api.getRequest(url)).toEqual(fetch(url));
    });
});