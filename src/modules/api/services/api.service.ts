/**
 * @description Handles Api request
 */
export class APIService {
    /*@ngInject*/
    constructor() {};

    /**
     * @desc handles GET api request and formatted for json
     * @param url defines the url
     * @return GET Request promise
     */
    public getRequest(url: string): Promise<any> {
        return fetch(url).then((response: any) => {
                return response.json();
            }).catch((error) => {

            });
    }
}