import {PredictiveInputController} from './predictive-input.controller';

/**
 * @description Input which provides a predictive auto complete list which a user may select from to help complete their entry
 */
export class PredictiveInput {
    public bindings: any;
    public template: any;
    public controller: any;

    /** 
     * @description Bindings
     * data {any} defines json data representing list of items to search from for autocomplete logic
     * cityId {number} defines corresponding city id of user's search
     * model {string} defines user's current input
     */
    constructor() {
        this.bindings = {
            data: '<',
            cityId: '=',
            model: '='
        };
        this.controller = PredictiveInputController;
        this.template =  require('../views/predictive-input.html');
    }
}
