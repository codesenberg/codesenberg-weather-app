/**
 * @description Controller for PredictiveInput Component
 */
export class PredictiveInputController {
    public data: any;
    public model: string = '';
    public showPredictive: boolean;
    public cityId: number;

    /*@ngInject*/
    constructor(private $timeout: any) {
        //bind to scope
        this.filter = this.filter.bind(this);
    }

    /** 
     * @description filters predicitve list
     * @param item defines the current item in list being filtered
     * @return if item should be filtered or not
     */
    public filter(item: CityItem): boolean {
        return item.name.toLowerCase().indexOf(this.model.toLowerCase().trim()) !== - 1;
    }

    /** 
     * @description listens to input entry and toggles the predictive list on
     */
    public onKeyDown(): void {
        this.showPredictive = true;
        this.cityId = undefined;
    }

    /** 
     * @description listens to when input loses focus and hides predictive list
     */
    public blur(): void {
        //add delay to accomodate for if user is selecting item from predictive list
        this.$timeout(() => {
            this.showPredictive = false;
        }, 200);
    }

    /** 
     * @description select item from predcitive list and update input
     * @param item defines item selected from list
     */
    public selectItem(item: CityItem): void {
        this.model = item.name;
        this.cityId = item.id;
        this.showPredictive = false;
    }
}
