declare function require(string: string): string;


declare interface ForecastData {
    city: any;
    current: any;
    get5day: any[];
}

declare interface WindObj {
    cardinal?: string;
    deg: number;
    speed: number
}

declare interface Rain {
    '3h': number
}
declare interface ForecastItem {
    dateToString: string;
    wind: WindObj;
    rainAccumulation: number;
    dt: number;
    dt_txt: string;
    main: any;
    weather: any;
    rain: Rain;
}

declare interface CityItem {
    id: number;
    name: string;
}