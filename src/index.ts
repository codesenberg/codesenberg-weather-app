import * as angular from 'angular';
import {AppContainer, ForecastService} from './modules/app-container';
import {APIService} from './modules/api';
import {ResultItem} from './modules/result-item';
import {PredictiveInput} from './modules/predictive-input';

angular.module('app', [])
    .component('appContainer', new AppContainer())
    .component('resultItem', new ResultItem())
    .component('predictiveInput', new PredictiveInput())
    .service('$api', APIService)
    .service('$forecastService', ForecastService);
angular.bootstrap(document, ['app'], {});